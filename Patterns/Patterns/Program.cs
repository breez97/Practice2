﻿using Patterns.Order_Type_Factory_Method_.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
{
    class Program
    {
        static void Main(string[] args)
        {
            // Создание объекта системы доставки еды
            FoodDeliverySystem deliverySystem = new FoodDeliverySystem();

            //Создание заказов (Фабрика - Client)
            //Пример 1
            Console.WriteLine("Заказ 1");

            Client client_Alex = new ExpressOrderClient("Alex");

            deliverySystem.SetDeliveryStrategy(new FastDeliveryStrategy()); // Установка стратегии доставки
            deliverySystem.ProcessOrder(order: client_Alex.MakeOrder(5), applyIngredients: true, applyWrapping: true); // Задание параметров заказу

            Console.WriteLine();

            //Пример 2
            Console.WriteLine("Заказ 2");

            Client client_Ivan = new ExpressOrderClient("Ivan");

            deliverySystem.SetDeliveryStrategy(new MaxEfficiencyStrategy());
            deliverySystem.ProcessOrder(order: client_Ivan.MakeOrder(3), applyIngredients: false, applyWrapping: true);

            Console.WriteLine();

            //Пример 3
            Console.WriteLine("Заказ 3");
            Client client_Stepan = new LargeGroupOrderClient("Ivan");

            deliverySystem.SetDeliveryStrategy(new MaxEfficiencyStrategy());
            deliverySystem.ProcessOrder(order: client_Stepan.MakeOrder(6), applyIngredients: false, applyWrapping: false);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
{
    class FastDeliveryStrategy : IDeliveryStrategy
    {
        public void Deliver()
        {
            Console.WriteLine("Выполняется быстрая доставка");
        }
        public string GetDeliveryType()
        {
            return "Быстрая доставка";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
{
    class OptimalRouteStrategy : IDeliveryStrategy
    {
        public void Deliver()
        {
            Console.WriteLine("Выполняется доставка по оптимальному маршруту");
        }
        public string GetDeliveryType()
        {
            return "Доставка по оптимальному маршруту";
        }
    }
}

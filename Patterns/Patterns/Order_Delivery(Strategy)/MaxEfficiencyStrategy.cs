﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
{
    class MaxEfficiencyStrategy : IDeliveryStrategy
    {
        public void Deliver()
        {
            Console.WriteLine("Выполняется доставка с максимальной эффективностью");
        }
        public string GetDeliveryType()
        {
            return "Доставка с максимальной эффективностью";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
{
    class FoodDeliverySystem
    {
        private IDeliveryStrategy deliveryStrategy;

        public void SetDeliveryStrategy(IDeliveryStrategy strategy)
        {
            deliveryStrategy = strategy;
        }

        public void ProcessOrder(Order order, bool applyIngredients, bool applyWrapping)
        {
            order.Process();

            if (applyIngredients)
            {
                order = new IngredientsDecorator(order);
                order.Process();
            }

            if (applyWrapping)
            {
                order = new WrappingDecorator(order);
                order.Process();
            }

            order.ConfirmOrder();

            if (deliveryStrategy != null)
            {
                deliveryStrategy.Deliver();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
{
    class IngredientsDecorator : OrderDecorator
    {
        public IngredientsDecorator(Order order) : base(order)
        {
        }
        public override void Process()
        {
            Console.WriteLine("* Применение дополнительных ингредиентов доставки");
        }
        public override void ConfirmOrder()
        {
            base.ConfirmOrder();
        }
    }
}

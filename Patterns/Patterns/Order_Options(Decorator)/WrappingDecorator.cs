﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
{
    class WrappingDecorator : OrderDecorator
    {
        public WrappingDecorator(Order order) : base(order)
        {
        }
        public override void Process()
        {
            Console.WriteLine("* Применение необычной упаковки");
        }
        public override void ConfirmOrder()
        {
            base.ConfirmOrder();
        }
    }
}

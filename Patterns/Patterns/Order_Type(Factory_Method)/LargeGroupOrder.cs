﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
{
    class LargeGroupOrder : Order
    {
        public override void Process()
        {
            Console.WriteLine("Обработка заказа на большую компанию");
        }
        public override void ConfirmOrder()
        {
            Console.WriteLine("Подтверждение заказа на большую компанию");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Order_Type_Factory_Method_.Clients
{
    public class RegularOrderClient : Client
    {
        public RegularOrderClient(string name) : base(name)
        {
            Console.WriteLine($"Создан обычный заказ на имя: {name}");
        }
        public override Order MakeOrder(int countDishes)
        {
            Console.WriteLine($"Создан обычный заказ c {countDishes} блюдами");
            return new RegularOrder();
        }
    }
}

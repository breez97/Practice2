﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Order_Type_Factory_Method_.Clients
{
    public class LargeGroupOrderClient : Client
    {
        public LargeGroupOrderClient(string name) : base(name)
        {
            Console.WriteLine($"Создан заказ на большую компанию на имя: {name}");
        }
        public override Order MakeOrder(int countDishes)
        {
            Console.WriteLine($"Создан заказ на большую компанию c {countDishes} блюдами");
            return new LargeGroupOrder();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Order_Type_Factory_Method_.Clients
{
    public class ExpressOrderClient : Client
    {
        public ExpressOrderClient(string name) : base(name)
        {
            Console.WriteLine($"Создан экспресс заказ на имя: {name}");
        }
        public override Order MakeOrder(int countDishes)
        {
            Console.WriteLine($"Создан экспресс заказ c {countDishes} блюдами");
            return new ExpressOrder();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Order_Type_Factory_Method_.Clients
{
    public abstract class Client
    {
        private string _name;
        public string Name => _name;
        public Client(string name)
        {
        }
        public abstract Order MakeOrder(int countDishes);
    }
}

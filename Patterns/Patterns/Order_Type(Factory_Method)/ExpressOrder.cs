﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
{
    class ExpressOrder : Order
    {

        public override void Process()
        {
            Console.WriteLine("Обработка экспресс заказа");
        }
        public override void ConfirmOrder()
        {
            Console.WriteLine("Подтверждение экспресс заказа");
        }
    }
}

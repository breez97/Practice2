﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
{
    class RegularOrder : Order
    {
        public override void Process()
        {
            Console.WriteLine("Обработка обычного заказа");
        }
        public override void ConfirmOrder()
        {
            Console.WriteLine("Подтверждение обычного заказа");
        }
    }
}

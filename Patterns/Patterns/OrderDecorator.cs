﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
{
    abstract class OrderDecorator : Order
    {
        protected Order order;

        public OrderDecorator(Order order)
        {
            this.order = order;
        }

        public override void Process()
        {
            order.Process();
        }

        public override void ConfirmOrder()
        {
            order.ConfirmOrder();
        }
    }
}
